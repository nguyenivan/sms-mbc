from google.appengine.ext import db

class Logs(db.Model):
	#des: save log for setting tone
	phone_no = db.StringProperty()
	action = db.StringProperty()
	app_name = db.StringProperty()
	time = db.DateTimeProperty()

class Links(db.Model):
	#des: store link and count number of download, status of link
	link = db.StringProperty()
	app_name = db.StringProperty()
	created = db.DateTimeProperty()
	downloaded = db.IntegerProperty()
    
class FileApp(db.Model):
	#des: 
	filekey = db.StringProperty()
	app_name = db.StringProperty()
	version = db.StringProperty()
	num_down = db.IntegerProperty()