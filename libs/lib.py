﻿
import re

INTAB = "ạảãàáâậầấẩẫăắằặẳẵóòọõỏôộổỗồốơờớợởỡéèẻẹẽêếềệểễúùụủũưựữửừứíìịỉĩýỳỷỵỹđ"
INTAB = [ch.encode('utf8') for ch in unicode(INTAB, 'utf8')]

OUTTAB = "a"*17 + "o"*17 + "e"*11 + "u"*11 + "i"*5 + "y"*5 + "d"

retab = re.compile("|".join(INTAB))
replaces_dict = dict(zip(INTAB, OUTTAB))

def khongdau(utf8_str):
    return retab.sub(lambda m: replaces_dict[m.group(0)], utf8_str.encode('utf8'))

def k(key, val):
	return '"'+key+'":"'+val+'"'

def write(key,val):
	return '"'+key+'":"'+val+'"'

def parse_obj(item):
	out = "{"
	out += k("name",item.name) + ","
	out += k("description",item.description) + ","
	out += k("type",item.type) + ","
	out += k("district",item.district) + ","
	out += k("city",item.city) + ","
	out += k("lat",item.lat) + ","
	out += k("lon",item.lon) + ","
	out += k("address",item.address) + ","
	out += k("link",item.link) + ","
	out += k("zoom",item.zoom) + ","
	out += k("investor",item.investor)
	out += "}"
	return out

def json_encode(projects):
	out = "["
	n = len(projects)
	i = 0
	for item in projects:
		out += parse_obj(item)
		i=i+1
		if i<n:
			out += ', '
	out += "]"
	return out

def parse_prov(item):
	out = '{"provider":{'
	out += write("name",item.name) + ","
	out += write("id",item.id) 
	out += "}}"
	return out

def parse_genr(item):
	out = '{"genre":{'
	out += write("name",item.name) + ","
	out += write("id",item.id) 
	out += "}}"
	return out

def parse_arts(item):
	out = '{"artist":{'
	out += write("name",item.name) + ","
	out += write("id",item.id) 
	out += "}}"
	return out

def parse_song(item):
	out = '{"song":{'
	out += write("name",item.name) + ","
	out += write("id",item.id) + ","
	out += write("artist_id",item.artist_id) 
	out += "}}"
	return out