from fibo import listLaMis, listBattleTank, listNews, listSkyDoor, \
	genResponseXML, listRingBack, listTankFullA, listTankFullB, \
	listTankHueAC, listTankHueB, listTankDaNangAC, listTankDaNangB, \
	listTankSaiGonAC, listTankSaiGonB, listTankOnline
from google.appengine.api import mail, memcache, urlfetch, urlfetch, \
	users
from google.appengine.ext import blobstore, db, webapp
from google.appengine.ext.webapp import blobstore_handlers, template, \
	util
from google.appengine.ext.webapp.util import run_wsgi_app
from models import FileApp, Links, Logs
import base64
import datetime
import md5
import os
import random
import struct
import sys
import urllib
import uuid




rooturl = 'http://sms.mobicom.vn/'
listDL = ['mbclamisassf434dsfsedf.jar', 'mbctank1975dfsf24465dsf.jar', 'mbcbattletankdsfs6545sd4fds.jar', 'mbcnewsh7fldha47f4d62qF23kj.jar', 'mbctankhuesdf754hd64hweh484.jar', 'mbctankdanangh6gd327ifj67dh4.jar', 'mbctanksaigonh6d4k7h7hf395j.jar']

class MainHandler(webapp.RequestHandler):
	def get(self):
		self.response.out.write("It works!")

class ComHandler(webapp.RequestHandler):
	def get(self):
		message = urllib.unquote(self.request.get("message"))
		phone = urllib.unquote(self.request.get("phone"))
		service = urllib.unquote(self.request.get("service"))
		port = urllib.unquote(self.request.get("port"))
		main = urllib.unquote(self.request.get("main"))
		sub = urllib.unquote(self.request.get("sub"))
		md5id = str(uuid.uuid4()) 	#id duy nhat gan cho tin tra ve
		
		kq = '<ClientResponse><Message><PhoneNumber>' + phone
		kq += '</PhoneNumber><Message>'+message+'</Message><SMSID>' + md5id
		kq += '</SMSID><ServiceNo>' + service
		kq += '</ServiceNo></Message></ClientResponse>'
		''' not need to reply a message'''
		self.response.out.write(kq)
		
class ActivationHandler(webapp.RequestHandler):
	def get(self):
		# get param from FIBO
		message = urllib.unquote(self.request.get("message"))
		phone = urllib.unquote(self.request.get("phone"))
		service = urllib.unquote(self.request.get("service"))
		port = urllib.unquote(self.request.get("port"))
		main = urllib.unquote(self.request.get("main"))
		sub = urllib.unquote(self.request.get("sub"))
		
		md5id = str(uuid.uuid4()) 	#id duy nhat gan cho tin tra ve
		
		kq = '<ClientResponse><Message><PhoneNumber>' + phone
		kq += '</PhoneNumber><Message>Ban da kich hoat thanh cong</Message><SMSID>' + md5id
		kq += '</SMSID><ServiceNo>' + service
		kq += '</ServiceNo></Message></ClientResponse>'
		''' not need to reply a message'''
		self.response.out.write(kq)

class GenKeyHandler(webapp.RequestHandler):
	def get(self):
		# get param from FIBO
		message = urllib.unquote(self.request.get("message"))
		phone = urllib.unquote(self.request.get("phone"))
		service = urllib.unquote(self.request.get("service"))
		port = urllib.unquote(self.request.get("port"))
		main = urllib.unquote(self.request.get("main"))
		sub = urllib.unquote(self.request.get("sub"))
		# end get param from FIBO
		md5id = str(uuid.uuid4()) 	#id duy nhat gan cho tin tra ve
		
		now = datetime.datetime.now()
		dd = now.day
		mm = now.month
		yy = now.year
		rd = random.randint(0, 99)
		
		app = 'Ung Dung'
		
		message = message.upper()
		main = main.upper()
		sub = sub.upper()
		if listLaMis.count(sub) > 0 :
			app = 'lamis' 
		elif listTankFullA.count(sub) > 0 :
			app = 'tank1975_full_a'
		elif listTankFullB.count(sub) > 0 :
			app = 'tank1975_full_b'
		elif listTankHueAC.count(sub) > 0 :
			app = 'tank1975_hue_ac'
		elif listTankHueB.count(sub) > 0 :
			app = 'tank1975_hue_b'
		elif listTankDaNangAC.count(sub) > 0 :
			app = 'tank1975_danang_ac'
		elif listTankDaNangB.count(sub) > 0 :
			app = 'tank1975_danang_b'
		elif listTankSaiGonAC.count(sub) > 0 :
			app = 'tank1975_saigon_ac'
		elif listTankSaiGonB.count(sub) > 0 :
			app = 'tank1975_saigon_b'
		elif listBattleTank.count(sub) > 0 :
			app = 'battletank'
		elif listNews.count(sub) > 0 :
			app = 'news'
		elif listSkyDoor.count(sub) > 0 :
			app = 'skydoor'
		elif listRingBack.count(sub) > 0:
			app = 'ringback'
		elif listTankOnline.count(sub) > 0:
			app = 'tankonline'

		http = rooturl + 'dl/'
		tmpphone = phone[-6:]
		code = encode(int(tmpphone + str(dd) + str(mm) + str(yy) + str(rd))) + '.jar'
		if app == 'skydoor':
			link = "http://skydoor.net/mobile/MobiTravel.jar"
		elif app == 'tankonline':
			link = 'http://goo.gl/os3rw'
		else:
			link = http + code
			
		trave = genResponseXML(message, phone, service, port, main, sub, link, md5id)
		#add data to datastore to check if there are any download request
		mlk = Links()
		mlk.link = code
		mlk.app_name = app
		mlk.created = now
		mlk.downloaded = 0
		mlk.put()
		# update log to note
		writelog(phone, 'Request Link', app)
		# response link to client
		self.response.out.write(trave)
		
class DownloadHandler(blobstore_handlers.BlobstoreDownloadHandler):
	def get(self, resource):
		mlink = urllib.unquote(resource)
		if (listDL.count(mlink) < 1): #neu k phai direct link
			l = Links.gql("WHERE link = :1", mlink).get()
			if l == None :
				self.response.out.write("Duong link khong hop le.")
			elif int(l.downloaded) > 1 :
				self.response.out.write("Ban da download qua so lan toi da.")
			else :
				mn = l.app_name
				if mn != '':
					f = FileApp.gql("WHERE app_name = :1", mn).get()
					if f != None :
						blobkey = f.filekey
						blob_info = blobstore.BlobInfo.get(blobkey)
						# update log to note
						writelog('xxxxxxxxxxx', 'User download', mn)
						# end write log
						self.send_blob(blob_info)
					else :
						raise RuntimeError , 'Khong co file tuong ung voi tin nhan.'

					# update download status link
					# This code for type detection - backward compatible with 'downloaded' field

					if type (l.downloaded) is int or type(l.downloaded) is long:
						l.downloaded = int(l.downloaded) + 1
					else:
						l.downloaded = str(int(l.downloaded) + 1)

					l.put()
					# end update
					# update number of download of file
					num = f.num_down + 1
					f.num_down = num
					f.put()
					#end update number of download file
		# xu ly khi co download link
		else:
			if mlink == listDL[0]:
				mn = 'lamis'
			elif mlink == listDL[1]:
				mn = 'tank1975'
			elif mlink == listDL[2]:
				mn = 'battletank'
			elif mlink == listDL[3]:
				mn = 'news'
			elif mlink == listDL[4]: 
				mn = 'tank-hue'
			elif mlink == listDL[5]: 
				mn = 'tank-danang'
			elif mlink == listDL[6]: 
				mn = 'tank-saigon'
			
			f = FileApp.gql("WHERE app_name = :1", mn).get()
			if f != None :
				blobkey = f.filekey
				blob_info = blobstore.BlobInfo.get(blobkey)
				# update log to note
				writelog('xxxxxxxxxxx', 'User download', mn)
				# end write log
				self.send_blob(blob_info)
			else :
				raise RuntimeError , 'Khong co file tuong ung voi tin nhan.'
			
			# update log to note
			writelog('xxxxxxxxxxx', 'Admin download', mn)
			# end write log
			self.send_blob(blob_info)
  
class AdminHandler(webapp.RequestHandler):
	def get(self):
		upload_url = blobstore.create_upload_url('/upload')
		self.response.out.write('<html><body>')
		self.response.out.write('<form action="%s" method="POST" enctype="multipart/form-data">' % upload_url)
		self.response.out.write("""Upload File: <input type="file" name="file"><br>Version: <input type="text" name="version"><br> App Name: <input type="text" name="app_name"><br><input type="submit" name="submit" value="Submit"> </form></body></html>""")

class UploadHandler(blobstore_handlers.BlobstoreUploadHandler):
	def post(self):
		upload_files = self.get_uploads('file')  # 'file' is file upload field in the form
		blob_info = upload_files[0]
		#self.redirect('/serve/%s' % blob_info.key())
		
		file_infor = FileApp()
		file_infor.filekey = str(blob_info.key())
		file_infor.app_name = self.request.get('app_name')
		file_infor.version = self.request.get('version')
		file_infor.num_down = 0
		file_infor.put()
		
		self.redirect("/")

class ServeHandler(blobstore_handlers.BlobstoreDownloadHandler):
	def get(self, resource):
		resource = str(urllib.unquote(resource))
		blob_info = blobstore.BlobInfo.get(resource)
		self.send_blob(blob_info)
############################################################################################################
#			FUNCTION
############################################################################################################
# function to encode
def encode(n):
	data = struct.pack('<Q', n).rstrip('\x00')
	if len(data) == 0:
		data = '\x00'
	s = base64.urlsafe_b64encode(data).rstrip('=')
	return s
	
#function to decode
def decode(s):
	data = base64.urlsafe_b64decode(s + '==')
	n = struct.unpack('<Q', data + '\x00' * (8 - len(data)))
	return n[0]

# function to save log for request
def writelog(phone, action, appname):
	mlog = Logs()
	now = datetime.datetime.now()
	
	mlog.phone_no = phone
	mlog.action = action
	mlog.app_name = appname
	mlog.time = now
	mlog.put()





# function main
def main():
	application = webapp.WSGIApplication([('/', MainHandler),
											('/gen.*', GenKeyHandler),
											('/com.*', ComHandler),
											('/act.*', ActivationHandler),
											('/dl/(.*)', DownloadHandler), 	
											('/upload', UploadHandler),
											('/admin', AdminHandler),
											('/serve/([^/]+)?', ServeHandler)
	], debug=True)
	util.run_wsgi_app(application)


if __name__ == '__main__':
	main()
