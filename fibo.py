import os
import sys
import logging

listIP = ['118.69.199.9', '112.78.7.18', '202.158.244.73', '203.171.30.222']

listTankFullA = ['T1', 'T2', 'T3', 'T4', 'T5'] 
listTankFullB = ['TF']

listLaMis8177 = ['LAMIS']
listLaMis8777 = ['X1', 'X2', 'X3', 'X4', 'X5']

listBattleTank8177 = []
listBattleTank8777 = ['B1', 'B2', 'B3', 'B4', 'B5']

listNews8177 = ['NEWS']
listNews8777 = ['N1', 'N2', 'N3', 'N4', 'N5']

listSkyDoor8177 = ['SKY']
listSkyDoor8777 = ['SKY']

listTankHueB = ['TH']
listTankHueAC = ['TH']

listTankDaNangB = ['TDN']
listTankDaNangAC = ['TDN']

listTankSaiGonB = ['TSG']
listTankSaiGonAC = ['TSG']

listRingBack8177 = ['RINGBACK']
listRingBack8777 = ['RB']

listTankOnline8177 = ['TO']
listTankOnline8777 = ['TO']

listLaMis = listLaMis8177 + listLaMis8777
listBattleTank = listBattleTank8177 + listBattleTank8777
listNews = listNews8177 + listNews8777
listSkyDoor = listSkyDoor8177 + listSkyDoor8777
listRingBack = listRingBack8777 + listRingBack8177
listTankOnline = listTankOnline8177 + listTankOnline8777

list8177 = listTankFullB + listLaMis8177 + listBattleTank8177 + listNews8177 + listSkyDoor8177 + listTankHueB + listTankDaNangB + listTankSaiGonB + listRingBack8177 + listTankOnline8177
list8777 = listTankFullA + listLaMis8777 + listBattleTank8777 + listNews8777 + listSkyDoor8777 + listTankHueAC + listTankDaNangAC + listTankSaiGonAC + listRingBack8777 + listTankOnline8777

listKEYSUB = list8177 + list8777

listPORT = ['8177', '8777']

def CheckServer(self):
	#check valid IP to ensure request sent by FIBO
	ip = self.request.remote_addr
	ipOK = (listIP.count(ip) >= 1)
	
	if (not ipOK):
		self.response.out.write(ip + ' can not access')
		sys.exit()

def genResponseXML(message, phone, service, port, main, sub, link, md5id):
	kq = ''
	dtll = '0904480237'
	#if port != '8177':  # neu khac dau so
	if (listPORT.count(port) < 1):	
		kq = '<ClientResponse><Message><PhoneNumber>' + phone
		kq += '</PhoneNumber><Message>Ban da nhan sai dau so. Vui long thu lai voi dau so 8777. DT ho tro: ' + dtll + '</Message><SMSID>' + md5id
		kq += '</SMSID><ServiceNo>' + service
		kq += '</ServiceNo></Message></ClientResponse>'
	else:
		#xu ly tin
		main = main.upper()
		sub = sub.upper()
		
		#######################
		helpText = '<ClientResponse><Message><PhoneNumber>' + phone
		helpText += '</PhoneNumber><Message>Ban da nhan sai cu phap. Soan MBC T1 de cai game Dai Thang Mua Xuan. Soan MBC X1 gui 8777 de cai game Cuc Gach Thong Minh. Hay goi ' + dtll + ' de duoc ho tro.</Message><SMSID>' + md5id
		helpText += '</SMSID><ServiceNo>' + service
		helpText += '</ServiceNo></Message></ClientResponse>'
		#######################
		wrongSyntax = True
		logging.info(main + ' ' + sub)
		if main == "MBC":
			if ((list8177.count(sub) and port == "8177") or (list8777.count(sub) and port == "8777")):
				#tmplamis = 'Tro choi xep gach/Tetrix noi tieng da tro lai voi nhieu che do va cach choi hap dan hon.'
				#tmptank1975 = 'Tro choi dai chien xe tang tai hien lai chien thang lich su mua xuan 30 thang 4 cua dan toc ta.'
				#tmpbattletank = ''
				#tmpNews = 'Ung dung doc bao tren dien thoai cuc ki tien loi.'
				if listLaMis.count(sub) > 0 : #game lamis
					title = 'CUC GACH THONG MINH:'
				elif listTankFullA.count(sub) > 0 or listTankFullB.count(sub) > 0 or listTankHueAC.count(sub) or listTankHueB.count(sub)or listTankDaNangAC.count(sub) or listTankDaNangB.count(sub)or listTankSaiGonAC.count(sub) or listTankSaiGonB.count(sub):
					title = 'DAI THANG MUA XUAN:'
				elif listBattleTank.count(sub) > 0 :
					title = 'DAI CHIEN XE TANG:'
				elif listNews.count(sub) > 0:
					title = 'DOC BAO MOBI:'
				elif listSkyDoor.count(sub) > 0:
					title = 'SkyDoor:'				
				elif listRingBack.count(sub) > 0:
					title = 'NHAC CHO:'
				elif listTankOnline.count(sub) > 0:
					title = 'Tank Online:'
				
				kq = '<ClientResponse><Message><PhoneNumber>' + phone
				kq += '</PhoneNumber><Message>' + title + link
				kq += '</Message><SMSID>' + md5id
				kq += '</SMSID><ServiceNo>' + service
				kq += '</ServiceNo><ContentType>8</ContentType></Message></ClientResponse>'
				wrongSyntax = False
		if wrongSyntax == True:			
		# sai cu phap, huong dan nhan lai
			kq = helpText
	return kq
